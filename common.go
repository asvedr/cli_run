package cli_run

import (
	"fmt"
	"strings"
)

type err_not_found struct {
	key string
}

func (e err_not_found) Error() string {
	return fmt.Sprintf("param %s not found", e.key)
}

func GetParam(
	args []string,
	key string,
) (string, error) {
	for i, arg := range args {
		if strings.Compare(arg, key) != 0 {
			continue
		}
		if i+1 == len(args) {
			return "", &err_not_found{key: key}
		}
		val := args[i+1]
		return val, nil
	}
	return "", &err_not_found{key: key}
}

func GetParamDefault(
	args []string,
	key string,
	dflt string,
) (string, error) {
	for i, arg := range args {
		if strings.Compare(arg, key) != 0 {
			continue
		}
		if i+1 == len(args) {
			return "", &err_not_found{key: key}
		}
		val := args[i+1]
		return val, nil
	}
	return dflt, nil
}

func GetParamMap[T any](
	args []string,
	key string,
	fmap func(string) (T, error),
) (*T, error) {
	s_val, err := GetParam(args, key)
	if err != nil {
		return nil, err
	}
	val, err := fmap(s_val)
	return &val, err
}

func GetParamMapDefault[T any](
	args []string,
	key string,
	fmap func(string) (T, error),
	dflt T,
) (T, error) {
	s_val, err := GetParam(args, key)
	if err != nil {
		_, casted := err.(*err_not_found)
		if casted {
			return dflt, nil
		} else {
			return dflt, err
		}
	}
	return fmap(s_val)
}

func GetFlag(args []string, key string) bool {
	for _, arg := range args {
		if strings.Compare(arg, key) == 0 {
			return true
		}
	}
	return false
}
