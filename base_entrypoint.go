package cli_run

import "errors"

type BaseEntrypoint struct{}

type no_args_parser struct{}

func (BaseEntrypoint) Command() string {
	return ""
}

func (BaseEntrypoint) Description() string {
	return ""
}

func (BaseEntrypoint) Parser() IArgParser {
	return &no_args_parser{}
}

func (no_args_parser) Parse(args []string) (any, error) {
	if len(args) != 0 {
		return nil, errors.New("no args expected")
	}
	return nil, nil
}

func (no_args_parser) Params() []string {
	return []string{}
}
