package cli_run

import (
	"fmt"
	"os"
	"reflect"
	"strings"
)

type Runner struct {
	command       string
	description   string
	use_cases     []IEntrypoint
	main_use_case IEntrypoint
}

type runner_parser struct {
	runner *Runner
}

type runner_request struct {
	prog string
	args []string
}

func NewRunner() IRunner {
	return &Runner{}
}

/// impl IRunner methods

func (self *Runner) SetDescription(descr string) IRunner {
	self.description = descr
	return self
}

func (self *Runner) AddUseCase(use_case IEntrypoint) IRunner {
	self.use_cases = append(self.use_cases, use_case)
	return self
}

func (self *Runner) SetUseCase(use_case IEntrypoint) IRunner {
	self.main_use_case = use_case
	return self
}

func (self *Runner) SetCommand(command string) IRunner {
	self.command = command
	return self
}

func (Runner) run_use_case(use_case IEntrypoint, prog string, args []string) {
	if reflect.DeepEqual(args, []string{"-h"}) {
		println(prepare_cmd_help(prog, use_case))
		os.Exit(0)
	}
	request, err := use_case.Parser().Parse(args)
	if err != nil {
		msg := fmt.Sprintf("Can not parse args: %v\n", err)
		os.Stderr.WriteString(msg)
		os.Exit(1)
	}
	err = use_case.Execute(prog, request)
	if err != nil {
		msg := fmt.Sprintf("Failed: %v\n", err)
		os.Stderr.WriteString(msg)
		os.Exit(1)
	}
}

func (self *Runner) run_with(prog string, args []string) {
	if self.main_use_case != nil {
		self.run_use_case(self.main_use_case, prog, args)
		return
	}
	if len(args) == 0 || strings.Compare(args[0], "-h") == 0 {
		help := prepare_help(prog, self.description, self.use_cases)
		println(help)
		return
	}
	cmd := args[0]
	args = args[1:]
	use_case := self.choose_use_case(cmd)
	if use_case == nil {
		fmt.Printf("Command not found: %s\n", cmd)
		os.Exit(1)
	}
	self.run_use_case(use_case, prog+" "+cmd, args)
}

func (self *Runner) Run() {
	prog := os.Args[0]
	args := os.Args[1:]
	self.run_with(prog, args)
}

func (self *Runner) choose_use_case(cmd string) IEntrypoint {
	for _, uc := range self.use_cases {
		if uc.Command() == cmd {
			return uc
		}
	}
	return nil
}

/// impl IEntrypoint methods

func (self *Runner) Command() string {
	return self.command
}

func (self *Runner) Description() string {
	return self.description
}

func (self *Runner) Parser() IArgParser {
	return &runner_parser{runner: self}
}

func (self *Runner) Execute(prog string, request any) error {
	self.run_with(prog, request.([]string))
	return nil
}

/// impl IArgParser methods

func (self *runner_parser) Parse(args []string) (any, error) {
	return args, nil
}

var empty_args []string = []string{}

func (self *runner_parser) Params() []string {
	return empty_args
}
