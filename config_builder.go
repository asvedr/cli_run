package cli_run

import (
	"fmt"
	"os"
	"strings"
)

type config_builder struct {
	params map[string]arg_param
	prefix string
}

func ConfigBuilder() IConfigBuilder {
	return &config_builder{
		params: make(map[string]arg_param),
	}
}

func (self *config_builder) AddFlag(name string, descr string) {
	self.params[name] = arg_param{
		is_flag: true,
		descr:   descr,
	}
}

func (self *config_builder) AddParam(name string, parser ICliArgParable, descr string) {
	self.params[name] = arg_param{
		parser: parser,
		descr:  descr,
	}
}

func (self *config_builder) AddDefaultParam(name string, parser ICliArgParable, descr string) {
	self.params[name] = arg_param{
		parser:      parser,
		descr:       descr,
		default_val: parser.Value(),
	}
}

func (self *config_builder) AddOptParam(name string, parser ICliArgParable, descr string) {
	self.params[name] = arg_param{
		parser: parser,
		is_opt: true,
		descr:  descr,
	}
}

func (self *config_builder) SetPrefix(prefix string) {
	self.prefix = prefix
}

func (self *config_builder) Parse() (ParserResult, error) {
	result := ParserResult{
		Named: make(map[string]any),
		Rest:  []any{},
	}
	for var_name, schema := range self.params {
		val := os.Getenv(self.prefix + var_name)
		var err error
		if len(val) == 0 {
			err = self.parse_var_not_set(&result, var_name, schema)
		} else {
			err = self.parse_var_val(&result, var_name, schema.parser, val)
		}
		if err != nil {
			return result, err
		}
	}
	return result, nil
}

func (self *config_builder) Params() []string {
	var result []string
	for name, param := range self.params {
		attrs := []string{self.prefix + name + ":", param.descr}
		if param.is_flag {
			attrs = append(attrs, "(flag)")
		} else if param.is_opt {
			attrs = append(attrs, "(optional)")
		} else if param.default_val != nil {
			attrs = append(attrs, fmt.Sprintf("(default=%v)", param.default_val))
		}
		result = append(result, strings.Join(attrs, " "))
	}
	return result
}

func (self *config_builder) parse_var_not_set(
	result *ParserResult,
	key string,
	param arg_param,
) error {
	if param.default_val != nil {
		result.Named[key] = param.default_val
		return nil
	}
	if param.is_opt {
		return nil
	}
	return fmt.Errorf("env var %s%s not set", self.prefix, key)
}

func (self *config_builder) parse_var_val(
	result *ParserResult,
	key string,
	parser ICliArgParable,
	src string,
) error {
	val, err := parser.Parse(src)
	if err != nil {
		return fmt.Errorf("env var %s%s is invalid: %v", self.prefix, key, err)
	}
	result.Named[key] = val
	return nil
}
