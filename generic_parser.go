package cli_run

import (
	"fmt"
	"strings"
)

type generic_parser struct {
	params      map[string]arg_param
	rest_type   ICliArgParable
	rest_descr  string
	ignore_rest bool
}

type parser_wrapper struct{ parser *generic_parser }

type arg_param struct {
	is_flag     bool
	parser      ICliArgParable
	aliases     []string
	is_opt      bool
	default_val any
	descr       string
}

func GenericParser() IGenericParser {
	return &generic_parser{params: make(map[string]arg_param)}
}

func (self *generic_parser) AddFlag(names []string, descr string) {
	self.params[names[0]] = arg_param{
		is_flag: true,
		aliases: names,
		descr:   descr,
	}
}

func (self *generic_parser) AddParam(
	names []string,
	parser ICliArgParable,
	descr string,
) {
	self.params[names[0]] = arg_param{
		parser:  parser,
		aliases: names,
		descr:   descr,
	}
}

func (self *generic_parser) AddDefaultParam(
	names []string,
	parser ICliArgParable,
	descr string,
) {
	self.params[names[0]] = arg_param{
		aliases:     names,
		parser:      parser,
		is_opt:      true,
		default_val: parser.Value(),
		descr:       descr,
	}
}

func (self *generic_parser) AddOptParam(
	names []string,
	parser ICliArgParable,
	descr string,
) {
	self.params[names[0]] = arg_param{
		aliases: names,
		parser:  parser,
		is_opt:  true,
		descr:   descr,
	}
}

func (self *generic_parser) SetRestType(parser ICliArgParable, descr string) {
	self.rest_type = parser
	self.rest_descr = descr
}

func (self *generic_parser) IgnoreRest() {
	self.ignore_rest = true
}

func (self *generic_parser) Parse(args []string) (ParserResult, error) {
	result := ParserResult{
		Named: make(map[string]any),
		Rest:  []any{},
	}
	var err error
	for key, schema := range self.params {
		index := get_key_index(args, schema.aliases)
		if schema.is_flag {
			result.Named[key] = index >= 0
			if index >= 0 {
				args = append(args[:index], args[index+1:]...)
			}
		} else if index < 0 {
			err = self.parse_arg_not_set(&result, key, schema)
		} else if index == len(args)-1 {
			return result, fmt.Errorf("key %s must contain value", key)
		} else {
			src_val := args[index+1]
			args = append(args[:index], args[index+2:]...)
			err = self.parse_arg_with_value(&result, key, schema.parser, src_val)
		}
		if err != nil {
			return result, err
		}
	}
	if self.rest_type != nil {
		err = self.parse_rest(&result, self.rest_type, args)
	} else if !self.ignore_rest && len(args) > 0 {
		err = fmt.Errorf("unexpected args: %v", args)
	}
	return result, err
}

func (self *generic_parser) IntoArgParser() IArgParser {
	return parser_wrapper{parser: self}
}

func (self parser_wrapper) Parse(args []string) (any, error) {
	return self.parser.Parse(args)
}

func (self parser_wrapper) Params() []string {
	var result []string
	for _, param := range self.parser.params {
		result = append(result, self.make_param_info(param))
	}
	if self.parser.rest_type != nil {
		result = append(result, self.make_rest_info())
	}
	return result
}

func (self parser_wrapper) make_param_info(param arg_param) string {
	names := strings.Join(param.aliases, " ")
	if param.is_flag {
		return fmt.Sprintf("%s: %s", names, param.descr)
	}
	info := param.parser.Info()
	return fmt.Sprintf("%s (%s): %s", names, info, param.descr)
}

func (self parser_wrapper) make_rest_info() string {
	return fmt.Sprintf("(%s)+: %s", self.parser.rest_type.Info(), self.parser.rest_descr)
}

func (self *generic_parser) parse_arg_not_set(
	result *ParserResult,
	key string,
	param arg_param,
) error {
	if param.default_val != nil {
		result.Named[key] = param.default_val
		return nil
	}
	if param.is_opt {
		return nil
	}
	return fmt.Errorf("param %s not set", key)
}

func (self *generic_parser) parse_arg_with_value(
	result *ParserResult,
	key string,
	parser ICliArgParable,
	src string,
) error {
	val, err := parser.Parse(src)
	if err != nil {
		return fmt.Errorf("param %s is invalid: %v", key, err)
	}
	result.Named[key] = val
	return nil
}

func (self *generic_parser) parse_rest(
	result *ParserResult,
	parser ICliArgParable,
	args []string,
) error {
	for _, arg := range args {
		val, err := parser.Parse(arg)
		if err != nil {
			return fmt.Errorf("positional param is invalid: %v", err)
		}
		result.Rest = append(result.Rest, val)
	}
	return nil
}

func get_key_index(args []string, aliases []string) int {
	for index, arg := range args {
		for _, alias := range aliases {
			if arg == alias {
				return index
			}
		}
	}
	return -1
}
