package cli_run

import "sync"

type Lazy[T any] struct {
	once  sync.Once
	value T
	Func  func() T
}

func (self *Lazy[T]) Force() T {
	self.once.Do(func() { self.value = self.Func() })
	return self.value
}
