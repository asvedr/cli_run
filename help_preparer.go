package cli_run

import (
	"fmt"
	"strings"
)

func prepare_help(prog string, descr string, eps []IEntrypoint) string {
	lines := []string{fmt.Sprintf("%s - %s\nCommands:", prog, descr)}
	for _, ep := range eps {
		lines = append(lines, prepare_uc(prog, ep))
	}
	return strings.Join(lines, "\n")
}

func prepare_cmd_help(prog string, ep IEntrypoint) string {
	cmd := ep.Command()
	if len(cmd) != 0 {
		prog = prog + " " + cmd
	}
	lines := []string{fmt.Sprintf("%s - %s\nParams:", prog, ep.Description())}
	for _, param := range ep.Parser().Params() {
		lines = append(lines, fmt.Sprintf("  %s", param))
	}
	return strings.Join(lines, "\n")
}

func prepare_uc(prog string, ep IEntrypoint) string {
	var lines []string
	for _, param := range ep.Parser().Params() {
		lines = append(lines, fmt.Sprintf("  %s", param))
	}
	txt := fmt.Sprintf("%s %s - %s", prog, ep.Command(), ep.Description())
	if len(lines) == 0 {
		return txt
	}
	return fmt.Sprintf("%s\n Params:\n%s", txt, strings.Join(lines, "\n"))
}
