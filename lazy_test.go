package cli_run_test

import (
	"testing"

	"gitlab.com/asvedr/cli_run"
)

func TestLazyLocal(t *testing.T) {
	var call_count int = 0
	l := cli_run.Lazy[string]{
		Func: func() string {
			call_count += 1
			return "abc"
		},
	}
	if call_count != 0 {
		t.Fatal(call_count)
	}
	forced := l.Force()
	if forced != "abc" {
		t.Fatal(forced)
	}
	if call_count != 1 {
		t.Fatal(call_count)
	}
	forced = l.Force()
	if forced != "abc" {
		t.Fatal(forced)
	}
	if call_count != 1 {
		t.Fatal(call_count)
	}
}

var global_call_count int = 0
var global_lazy cli_run.Lazy[string] = cli_run.Lazy[string]{
	Func: func() string {
		global_call_count += 1
		return "def"
	},
}

func TestLazyGlobal(t *testing.T) {
	if global_call_count != 0 {
		t.Fatal(global_call_count)
	}
	forced := global_lazy.Force()
	if forced != "def" {
		t.Fatal(forced)
	}
	if global_call_count != 1 {
		t.Fatal(global_call_count)
	}
	forced = global_lazy.Force()
	if forced != "def" {
		t.Fatal(forced)
	}
	if global_call_count != 1 {
		t.Fatal(global_call_count)
	}
}
