package cli_run

type IArgParser interface {
	Parse(args []string) (any, error)
	Params() []string
}

type IEntrypoint interface {
	Command() string
	Description() string
	Parser() IArgParser
	Execute(prog string, request any) error
}

type IRunner interface {
	IEntrypoint
	AddUseCase(use_case IEntrypoint) IRunner
	SetUseCase(use_case IEntrypoint) IRunner
	SetDescription(descr string) IRunner
	SetCommand(string) IRunner
	Run()
}

type ICliArgParable interface {
	Info() string
	Parse(src string) (any, error)
	Value() any
}

type ParserResult struct {
	Named map[string]any
	Rest  []any
}

type IGenericParser interface {
	AddFlag(names []string, descr string)
	AddParam(names []string, parser ICliArgParable, descr string)
	AddDefaultParam(names []string, parser ICliArgParable, descr string)
	AddOptParam(names []string, parser ICliArgParable, descr string)
	SetRestType(parser ICliArgParable, descr string)
	IgnoreRest()
	Parse(args []string) (ParserResult, error)
	IntoArgParser() IArgParser
}

type IConfigBuilder interface {
	AddFlag(name string, descr string)
	AddParam(name string, parser ICliArgParable, descr string)
	AddDefaultParam(name string, parser ICliArgParable, descr string)
	AddOptParam(name string, parser ICliArgParable, descr string)
	SetPrefix(prefix string)
	Parse() (ParserResult, error)
	Params() []string
}
