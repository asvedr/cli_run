package cli_run

import (
	"fmt"
	"strconv"
)

type Int struct {
	Default int64
	Gt      *int64
	Lt      *int64
	Choices []int64
}
type Float struct {
	Default float64
	Gt      *float64
	Lt      *float64
}
type Str struct {
	Default string
	Choices []string
}

func (self Int) Info() string {
	if len(self.Choices) != 0 {
		return fmt.Sprintf("one of %v", self.Choices)
	}
	if self.Gt != nil || self.Lt != nil {
		gt := ".."
		lt := ".."
		if self.Gt != nil {
			gt = fmt.Sprint(*self.Gt)
		}
		if self.Lt != nil {
			lt = fmt.Sprint(*self.Lt)
		}
		return fmt.Sprintf("%s < integer < %s", gt, lt)
	}
	return "integer"
}

func (self Int) Parse(src string) (any, error) {
	val, err := strconv.ParseInt(src, 10, 64)
	if err != nil {
		return nil, err
	}
	if len(self.Choices) > 0 {
		for _, variant := range self.Choices {
			if variant == val {
				return val, nil
			}
		}
		return nil, fmt.Errorf("must be elem of %v", self.Choices)
	}
	if self.Gt != nil && val <= *self.Gt {
		return nil, fmt.Errorf("must be > %d", *self.Gt)
	}
	if self.Lt != nil && val >= *self.Lt {
		return nil, fmt.Errorf("must be < %d", *self.Lt)
	}
	return val, nil
}

func (self Int) Value() any {
	return self.Default
}

func (self Float) Info() string {
	if self.Gt != nil || self.Lt != nil {
		gt := ".."
		lt := ".."
		if self.Gt != nil {
			gt = fmt.Sprint(*self.Gt)
		}
		if self.Lt != nil {
			lt = fmt.Sprint(*self.Lt)
		}
		return fmt.Sprintf("%s < rational < %s", gt, lt)
	}
	return "rational number"
}

func (self Float) Parse(src string) (any, error) {
	val, err := strconv.ParseFloat(src, 64)
	if err != nil {
		return nil, err
	}
	if self.Gt != nil && val < *self.Gt {
		return nil, fmt.Errorf("must be > %f", *self.Gt)
	}
	if self.Lt != nil && val > *self.Lt {
		return nil, fmt.Errorf("must be < %f", *self.Lt)
	}
	return val, nil
}

func (self Float) Value() any {
	return self.Default
}

func (self Str) Info() string {
	if len(self.Choices) != 0 {
		return fmt.Sprintf("one of %v", self.Choices)
	}
	return "string"
}
func (self Str) Parse(src string) (any, error) {
	if len(self.Choices) > 0 {
		for _, variant := range self.Choices {
			if variant == src {
				return src, nil
			}
		}
		return nil, fmt.Errorf("must be elem of %v", self.Choices)
	}
	return src, nil
}

func (self Str) Value() any {
	return self.Default
}
